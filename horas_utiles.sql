CREATE FUNCTION `horas_utiles`(starting_value DATETIME, end_value datetime) RETURNS time
BEGIN

	DECLARE horas int;
    DECLARE dias int;
    declare dias_utiles int;
    declare dias_calendario int;
    declare temporalDate int;
    declare dias_intermedios datetime;
    declare horas_con_formato time;
    declare segundos int;
    declare segundos_totales int;
    declare segundos_hasta_fin_dia int;
    declare segundos_desde_inicio_dia int;
	declare segundos_intermendios int;

    set horas = 0;
    set segundos = 0;
    set segundos_hasta_fin_dia = 0;
    set segundos_desde_inicio_dia = 0;
    set segundos_intermendios = 0;
    set dias = -1;
    set dias_utiles = 0;
    set dias_calendario = 0;
    set horas_hasta_fin_dia = 0;
    set horas_desde_inicio_dia = 0;
    set horas_intermedias = 0;

	
    set dias = TIMESTAMPDIFF(day,date(starting_value) , date(end_value));
    set dias_intermedios = starting_value;
	
    if date(starting_value) = date(end_value) then
			set segundos_totales = TIMESTAMPDIFF(SECOND,starting_value , end_value); 
	else
		WHILE  dias_calendario <= dias DO
			SET temporalDate = DAYOFWEEK(dias_intermedios);
            
            if temporalDate != 1 and temporalDate != 7 and date(dias_intermedios) != '2020-04-09' and date(dias_intermedios) != '2020-04-10' 
				and date(dias_intermedios) != '2020-05-01'
				then
                
				if date(starting_value) = date(dias_intermedios) then
					
                    set segundos_hasta_fin_dia = timestampdiff(SECOND,  starting_value, concat(date(starting_value),' 18:30:00'));
					if segundos_hasta_fin_dia < 0 then
						set segundos_hasta_fin_dia = 0;
					end if;
							
				elseif date(end_value) = date(dias_intermedios) then
					
					set segundos_desde_inicio_dia = timestampdiff(SECOND, concat(date(end_value),' 09:00:00'), end_value);
                    
                    if segundos_desde_inicio_dia > 30600 then /*segundos en 8 horas  30 min*/
						set segundos_desde_inicio_dia = 30600;
                        
					elseif segundos_desde_inicio_dia < 0 then
						set segundos_desde_inicio_dia = 0;
					end if;
                    
				else
					set segundos_intermendios = segundos_intermendios + dias_utiles * 30600 ;
				end if;
                
                set dias_utiles = dias_utiles + 1;
			end if;
            
			SET dias_intermedios = DATE_ADD(dias_intermedios, INTERVAL 1 DAY);
			SET dias_calendario = dias_calendario + 1;   
		END WHILE ;
        
        if date(DATE_ADD(starting_value, INTERVAL 1 DAY)) = date(end_value) then
			set segundos_totales =  segundos_hasta_fin_dia + segundos_desde_inicio_dia ;
		else
			set segundos_totales = segundos_hasta_fin_dia + segundos_desde_inicio_dia + segundos_intermendios ;
            
		end if;
        
    end if;
    
set horas_con_formato = sec_to_time(segundos_totales);
RETURN horas_con_formato;
END